package com.example.flows

import android.util.Log
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {
  // Only increment/decrement by 1 and use an intermediate operator to multiply
  // that by the value of the multiplier value.

  fun start() {
    Log.d(TAG, "initializing starting flow")
  }


  companion object {
    const val TAG = "Logger"
  }
}