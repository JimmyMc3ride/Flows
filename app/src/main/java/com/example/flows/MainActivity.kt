package com.example.flows

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import com.example.flows.ui.theme.FlowsTheme

class MainActivity : ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContent {
      val viewModel by viewModels<MainViewModel>()
      FlowsTheme {
        viewModel.start()
        Surface(
          modifier = Modifier.fillMaxSize(),
          color = MaterialTheme.colors.background
        ) {
          Column {
            Text(text = "Hello, world!")
            Text(text = "hex = ${Color(255, 255, 255).toHexCode()}")
          }
        }
      }
    }
  }
}

private fun Color.toHexCode(): String {
  val max = 255
  val red = this.red * max
  val green = this.green * max
  val blue = this.blue * max
  return String.format(
    "#%02x%02x%02x",
    red.toInt(),
    green.toInt(),
    blue.toInt()
  )
}
